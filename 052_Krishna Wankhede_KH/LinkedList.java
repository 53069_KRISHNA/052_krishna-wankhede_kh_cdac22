class LinkedList{
    static class Node{
	   int data;
	   Node next;
	   
      Node(int d){
	     data = d;
		 next = null;
	  }	   
	}
	static Node head = null;
	static Node tail = null;
	
	
	
	void addFirst(int d){
	   Node newNode = new Node(d);
	   if(head==null){
	      head = newNode;
		  tail = newNode;
	   
	   }
	   
	   else{
	   
	   newNode.next = head;
	   head = newNode;
	   
	   }
	}
	void display(){
	    Node n = head;
		if(n==null){
	       System.out.println("LIST IS EMPTY");
		   return;
		}
	    while(n!=null){
		  System.out.print(n.data+"--->");
		  n = n.next;
		}
	    System.out.print("NULL");
	
	}
	void reverseList(Node head){
		while(head.next!=null){
			reverseList(head.next);
			
		}
		System.out.print(head.data+"--->");
		System.out.print("NULL");
		
		
	}
	
	
	public static void main(String... args){
	    LinkedList l1 = new LinkedList();
		l1.addFirst(3);
		l1.addFirst(2);
		l1.addFirst(1);
		l1.display();
		l1.reverseList(head);
	
	
	}
	
	
}