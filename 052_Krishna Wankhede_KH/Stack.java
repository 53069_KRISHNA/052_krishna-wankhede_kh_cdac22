class Stack{
     int a[];
	 int size;
	 int top1,top2;
	 
	 Stack(int n){
	 
	    a = new int[n];
	    size = n;
        top1 = -1;
        top2 = size;		
	 }
	 
	 void push1(int d){
	      if(top1 < top2-1){
		     top1++;
			 a[top1] = d;
		  }
	      else{
		    System.out.println("STACK OVERFLOW");
		  }
	 }
     void push2(int d){
	    if(top1<top2-1){
		    top2--;
			a[top2] = d;
		}
		else{
		   System.out.println("STACK OVERFLOW");
		
		} 
	 }
	 
	 int pop1(){
	    if(top1>=0){
		   int j = a[top1];
		   top1--;
		   return j;
		}
	    else{
		  System.out.println("STACK UNDERFLOW");
		}
	    return 0;
	 }
	 int pop2(){
	    if(top2<size){
		  int k = a[top2];
		  top2++;
		  return k;
		}
	    else{
		   System.out.println("STACK UNDERFLOW");
		
		}
	      return 0;
	 }
	 
	 
	 
     public static void main(String... args){
	      Stack s1 = new Stack(5);
          s1.push1(5);
          s1.push2(10);
          s1.push2(15);	
          s1.push1(11);
          s1.push2(7);
		  System.out.println("Popped element from stack1 is "+s1.pop1());
          s1.push2(40);		  
		  System.out.println("Popped element from stack2 is "+s1.pop2());
	 
	 }

}