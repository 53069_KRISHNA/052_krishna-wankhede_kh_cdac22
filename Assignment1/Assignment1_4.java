//Swap two numbers without using third variable approach. 
import java.util.Scanner;
class Assignment1_4{
public static void main(String[] args)
{
int num1,num2;
Scanner sc = new Scanner(System.in);
System.out.println("Enter First value");
num1 = sc.nextInt();
System.out.println("Enter Second Value");
num2 = sc.nextInt();
System.out.println("Before swap value of num1 is "+num1+" and num2 is "+num2);

num1 = num1+num2;
num2 = num1-num2;
num1 = num1-num2;

System.out.println("After swap value of num1 is "+num1+" and num2 is "+num2);



}







}