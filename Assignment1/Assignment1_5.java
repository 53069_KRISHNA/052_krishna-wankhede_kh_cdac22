//How to check the given number is Positive or Negative in Java? 
import java.util.Scanner;
class Assignment1_5{
	public static void main(String[] args)
	{
		int number;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		number = sc.nextInt();
		
		if(number>0)
			System.out.println(number+" is positive number");
		else if(number<0)
			System.out.println(number+" is negative number");
		else
		System.out.println("0 is neither positive or negative");
		
		
		
	}
}